<!DOCTYPE html>

<%@ page import="java.net.URI" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="org.eclipse.lyo.oslc4j.core.model.Service" %>
<%@ page import="org.eclipse.lyo.oslc4j.core.model.ServiceProvider" %>
<%@ page import="org.eclipse.lyo.oslc4j.core.model.Dialog" %>
<%@ page import="org.eclipse.lyo.oslc4j.core.model.CreationFactory" %>
<%@page import="se.kth.md.assume.adaptors.gitlab.resources.JSPHelper" %>
<%@ page import="org.eclipse.lyo.oslc4j.core.model.QueryCapability" %>
<%--
Start of user code imports
--%>
<%--
End of user code
--%>

<%@ page contentType="text/html" language="java" pageEncoding="UTF-8" %>

<%
    ServiceProvider serviceProvider = (ServiceProvider)request.getAttribute("serviceProvider");
    Service[] services = (Service[])request.getAttribute("services");
%>
<%--
Start of user code getRequestAttributes
--%>
<%--
End of user code
--%>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><%= serviceProvider.getTitle() %></title>

    <link href="/import/bootstrap.min.css" rel="stylesheet">
    <link href="/import/sticky-footer-navbar.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <%--
Start of user code (RECOMMENDED) headStuff
    --%>
    <%--
End of user code
    --%>
</head>

<body>

<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">GitLab Adaptor</a>
        </div>
    </div>
</nav>

<!-- Begin page content -->
<div class="container">
    <div class="page-header">

        <%--
Start of user code (RECOMMENDED) bodyStuff1
        --%>
        <%--
End of user code
        --%>

        <h1><%= serviceProvider.getTitle() %></title></h1>
    </div>

    <!-- <p><%= serviceProvider.getDescription() %></p> -->

    <table>
        <tr>
            <%--
Start of user code (RECOMMENDED) bodyStuff2
            --%>
            <%--
End of user code
            --%>
        </tr>
    </table>

    <%for (int serviceIndex = 0; serviceIndex < services.length; serviceIndex++) {%>

    <h3>Service</h3>
    <% Dialog[] selectionDialogs = services[serviceIndex].getSelectionDialogs();%>
    <% if(selectionDialogs.length > 0) {%>
    <h4>Resource Selector Dialog(s)</h4>
    <%}%>
    <%
        for (int selectionDialogIndex = 0; selectionDialogIndex < selectionDialogs.length; selectionDialogIndex++) {
            String selectionDialog = selectionDialogs[selectionDialogIndex].getDialog().toString();
    %>
    <p>
        <a href="<%= selectionDialog %>"><%= selectionDialog %></a>
        (<a href="<%= request.getContextPath() %>/se/kth/md/assume/adaptors/gitlab/selectiondialogsampleclient.jsp?selectionUri=<%= URLEncoder.encode(selectionDialog.toString(), "UTF-8") %>">sample client</a>)
    </p>
    <%}%>

    <% Dialog[] creationDialogs = services[serviceIndex].getCreationDialogs(); %>
    <% if(creationDialogs.length > 0) {%>
    <h4>Resource Selector Dialog(s)</h4>
    <%}%>

    <%
        for (int creationDialogIndex = 0; creationDialogIndex < creationDialogs.length; creationDialogIndex++) {
            String creationDialog = creationDialogs[creationDialogIndex].getDialog().toString();
    %>
    <p>
        <a href="<%= creationDialog %>"><%= creationDialog %></a>
        (<a href="<%= request.getContextPath() %>/se/kth/md/assume/adaptors/gitlab/creationdialogsampleclient.jsp?creationUri=<%= URLEncoder.encode(creationDialog.toString(), "UTF-8") %>">sample client</a>)

    </p>
    <%}%>

    <% CreationFactory[] creationFactories = services[serviceIndex].getCreationFactories(); %>
    <% if(creationFactories.length > 0) {%>
    <h4>Resource Creation Factory(s)</h4>
    <%}%>
    <%
        for (int creationFactoryIndex = 0; creationFactoryIndex < creationFactories.length; creationFactoryIndex++) {
            String creationFactory = creationFactories[creationFactoryIndex].getCreation().toString();
    %>
    <p><a href="<%= creationFactory %>"><%= creationFactory %></a></p>
    <%}%>


    <% QueryCapability[] queryCapabilities= services[serviceIndex].getQueryCapabilities(); %>
    <% if(queryCapabilities.length > 0) {%>
    <h4>Resource Query Capability(s)</h4>
    <%}%>

    <%
        for (int queryCapabilityIndex = 0; queryCapabilityIndex < queryCapabilities.length; queryCapabilityIndex++) {
            String queryCapability = queryCapabilities[queryCapabilityIndex].getQueryBase().toString();
    %>

    <p><a href="<%= queryCapability %>"><%= queryCapability %></a></p>
    <%}%>

    <% if(creationFactories.length > 0 || queryCapabilities.length > 0) {%>
    <h4>Resource Shape(s)</h4>
    <%}%>
    <%
        for (int creationFactoryIndex = 0; creationFactoryIndex < creationFactories.length; creationFactoryIndex++) {
            URI[] creationShapes = creationFactories[creationFactoryIndex].getResourceShapes();
            String creationShape = creationShapes[0].toString();
    %>
    <p><a href="<%= creationShape %>"><%= creationShape %></a></p>
    <%}%>
    <%
        for (int queryCapabilityIndex = 0; queryCapabilityIndex < queryCapabilities.length; queryCapabilityIndex++) {
            String queryShape = queryCapabilities[queryCapabilityIndex].getResourceShape().toString();
    %>
    <p><a href="<%= queryShape %>"><%= queryShape %></a></p>
    <%}%>


    <%}%>
</div>

<footer class="footer">
    <div class="container">
        <p class="text-muted"><%= JSPHelper.getFooter()%></p>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/import/jquery.min.js"><\/script>')</script>
<script src="/import/bootstrap.min.js"></script>

</body>
</html>
