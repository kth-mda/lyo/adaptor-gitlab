<!DOCTYPE html>

<%@page import="org.eclipse.lyo.oslc4j.core.model.ServiceProvider"%>
<%@page import="org.eclipse.lyo.oslc4j.core.model.AbstractResource"%>
<%@page import="se.kth.md.assume.adaptors.gitlab.resources.JSPHelper"%>
<%@page import="java.util.List" %>
<%--
Start of user code imports
--%>
<%--
End of user code
--%>

<%@ page contentType="text/html" language="java" pageEncoding="UTF-8" %>

<%
  List<AbstractResource> resources = (List<AbstractResource>) request.getAttribute("resources");
  String queryUri = (String)request.getAttribute("queryUri");
  String nextPageUri = (String)request.getAttribute("nextPageUri");
%>
<%--
Start of user code getRequestAttributes
--%>
<%--
End of user code
--%>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Query Results</title>

  <link href="/import/bootstrap.min.css" rel="stylesheet">
  <link href="/import/sticky-footer-navbar.css" rel="stylesheet">
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <%--
Start of user code (RECOMMENDED) headStuff
    --%>
    <%--
End of user code
    --%>
</head>
<body>
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">GitLab Adaptor</a>
    </div>
  </div>
</nav>

<!-- Begin page content -->
<div class="container">
  <div class="page-header">
    <h1>Query Results</h1>
      <h5>Number of elements:&nbsp;
          <%= resources.size()%>
      </h5>

  </div>
        <%--
Start of user code (RECOMMENDED) bodyStuff1
        --%>
        <%--
End of user code
        --%>

        <% for (AbstractResource aResource : resources) { %>
        <p><%= JSPHelper.toHtml(aResource) %><br /></p>
        <% } %>

        <% if (nextPageUri != null) { %><a href="<%= nextPageUri %>">Next Page</a><% } %>

    </div>

<footer class="footer">
  <div class="container">
    <p class="text-muted"><%= JSPHelper.getFooter()%></p>
  </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/import/jquery.min.js"><\/script>')</script>
<script src="/import/bootstrap.min.js"></script>

</body>
</html>

