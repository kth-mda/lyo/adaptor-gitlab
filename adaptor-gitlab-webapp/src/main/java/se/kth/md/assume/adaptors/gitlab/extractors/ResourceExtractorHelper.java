package se.kth.md.assume.adaptors.gitlab.extractors;

import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.tools.store.ModelUnmarshallingException;
import org.eclipse.lyo.tools.store.Store;
import org.eclipse.lyo.tools.store.StoreAccessException;
import se.kth.md.assume.adaptors.gitlab.GitLabConstants;
import se.kth.md.assume.adaptors.gitlab.resources.ChangeRequest;
import se.kth.md.assume.adaptors.gitlab.resources.FileResource;
import se.kth.md.assume.adaptors.gitlab.resources.GitLabProject;
import se.kth.md.assume.adaptors.gitlab.servlet.ServiceProviderCatalogSingleton;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashSet;

/**
 * Created by Frederic Loiret on 2017-02-22.
 */
public class ResourceExtractorHelper {

    public static ChangeRequest createNewChangeRequest(String serviceProviderID,
                                                       int issueID,
                                                       String title,
                                                       String description,
                                                       Date createdAt) {
        Integer issueIDStr = new Integer(issueID);
        ChangeRequest cR = null;
        try {
            cR = new ChangeRequest(serviceProviderID, issueIDStr.toString());
            cR.setTitle(title);
            cR.setDescription(description);
            cR.setCreated(createdAt);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return cR;
    }

    public static void createAndSetCreatorURI (AbstractResource resource, String userName) {
        URI creatorURI;
        if (resource instanceof ChangeRequest) {
            ChangeRequest cR = (ChangeRequest)resource;
            try {
                creatorURI = new URI(GitLabConstants.GITLAB_PLATFORM_PREFIX_URL + userName);
                cR.setCreator(new Link(creatorURI));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        } else if (resource instanceof GitLabProject) {
            throw new UnsupportedOperationException("createAndSetCreatorURI not implemented for GitLabProject.");
        }
    }

    public static void addIssueToProject (GitLabProject project, int issueID) {
        HashSet<Link> issueLinks;
        if (project.getContainsChangeRequests() == null) {
            issueLinks = new HashSet<>();
        } else {
            issueLinks = new HashSet<>(project.getContainsChangeRequests());
        }
        Integer issueIDInt = new Integer(issueID);
        URI issueURI = ChangeRequest.constructURI(GitLabConstants.CHANGE_MANAGEMENT_SERVICE_PROVIDER_ID,
                issueIDInt.toString());
        issueLinks.add(new Link(issueURI));
        project.setContainsChangeRequests(issueLinks);
    }

    public static GitLabProject getGitLabProjectFromStore (Store store,
                                                           String serviceProviderId,
                                                           String resourceId) {
        GitLabProject aResource = null;
        URI serviceProviderURI = ServiceProviderCatalogSingleton.constructPmServicesServiceProviderURI(serviceProviderId);
        URI uriToRetrieve = GitLabProject.constructURI(serviceProviderId, resourceId);
        try {
            aResource = store.getResourceUnderKey(serviceProviderURI, uriToRetrieve, GitLabProject.class);
        } catch (StoreAccessException e) {
            e.printStackTrace();
        } catch (ModelUnmarshallingException e) {
            e.printStackTrace();
        }
        return aResource;
    }

    public static GitLabProject getGitLabProjectFromStore (Store store,
                                                           String serviceProviderId,
                                                           int resourceId) {
        Integer resourceIdStr = new Integer(resourceId);
        return getGitLabProjectFromStore (store, serviceProviderId, resourceIdStr.toString());
    }

    public static ChangeRequest getChangeRequestFromStore (Store store,
                                                           String serviceProviderId,
                                                           String resourceId) {
        ChangeRequest aResource = null;

        URI serviceProviderURI = ServiceProviderCatalogSingleton.constructCmServicesServiceProviderURI(serviceProviderId);
        URI uriToRetrieve = ChangeRequest.constructURI(serviceProviderId, resourceId);
        try {
            aResource = store.getResourceUnderKey(serviceProviderURI, uriToRetrieve, ChangeRequest.class);
        } catch (StoreAccessException e) {
            e.printStackTrace();
        } catch (ModelUnmarshallingException e) {
            e.printStackTrace();
        }
        return aResource;
    }

    public static ChangeRequest getChangeRequestFromStore (Store store,
                                                           String serviceProviderId,
                                                           int resourceId) {
        Integer i = new Integer(resourceId);
        return getChangeRequestFromStore(store, serviceProviderId, i.toString());
    }

    public static FileResource createNewFileResource(String serviceProviderID,
                                                      String fileID,
                                                      String title,
                                                      String description,
                                                      Date createdAt) {
        FileResource fr = null;
        try {
            fr = new FileResource(serviceProviderID, fileID);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        fr.setTitle(title);

        return fr;
    }

}
