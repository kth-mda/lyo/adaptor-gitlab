package se.kth.md.assume.adaptors.gitlab;

import com.jcraft.jsch.Session;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.*;
import org.eclipse.lyo.oslc4j.core.exception.OslcCoreApplicationException;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.Publisher;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import org.eclipse.lyo.tools.store.Store;
import org.eclipse.lyo.tools.store.StoreAccessException;
import org.eclipse.lyo.tools.store.update.StoreUpdateManager;
import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.GitlabGroup;
import org.gitlab.api.models.GitlabProject;
import org.gitlab.api.models.GitlabProjectMember;
import org.gitlab.api.models.GitlabUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.aide.repository.api.RepositoryAccessException;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.assume.adaptors.gitlab.extractors.ChangeRequestResourceExtractor;
import se.kth.md.assume.adaptors.gitlab.extractors.FileManagementResourceExtractor;
import se.kth.md.assume.adaptors.gitlab.extractors.GitLabProjectResourceExtractor;
import se.kth.md.assume.adaptors.gitlab.repository.GitRepository;
import se.kth.md.assume.adaptors.gitlab.repository.ResourceVersionHandler;
import se.kth.md.assume.adaptors.gitlab.resources.FileResource;
import se.kth.md.assume.adaptors.gitlab.resources.GitLabProject;
import se.kth.md.assume.adaptors.gitlab.resources.Person;
import se.kth.md.assume.adaptors.gitlab.servlet.FmServicesServiceProvidersFactory;
import se.kth.md.assume.adaptors.gitlab.servlet.ServiceProviderCatalogSingleton;
import se.kth.md.assume.adaptors.gitlab.updates.GitLabEventHandler;
import se.kth.md.assume.adaptors.gitlab.updates.GitlabChangeProviderImpl;
import se.kth.md.assume.adaptors.gitlab.updates.GitlabEventMessage;
import se.kth.md.assume.repository.update.RepositoryChangeProviderImpl;
import se.kth.md.assume.repository.update.RepositoryResourceManager;
import se.kth.md.assume.repository.update.RevisionMessage;
import se.kth.md.assume.repository.update.handlers.RevisionHandler;
import se.kth.md.assume.repository.update.handlers.TrsDiffHandler;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * Created by Frederic Loiret on 2017-02-21.
 */
public class GitLabManagerData {

    private static final Logger LOGGER = LoggerFactory.getLogger(GitLabProject.class);

    // This is the deployement time list of Gitlab projects to expose by the adaptor
    private HashSet<String> projectsToExpose;

    private String gitLabToken, getGitLabURL, adaptorBaseURL;
    private GitlabAPI gitlabAPI;

    // Key: Gitlab project name
    private HashMap<String, GitLabProjectInfos> gitLabProjects;
    private Store store;

    // Key: Gitlab user ID
    private HashMap<String, GitlabUser> gitLabUsers;
    private GitlabGroup gitlabGroup;

    private GitLabProjectResourceExtractor gitLabProjectExtractor;
    private ChangeRequestResourceExtractor changeRequestExtractor;

    private StoreUpdateManager<GitlabEventMessage> gitLabUpdateManager;

    public HashMap<String, GitLabProjectInfos> getGitLabProjects() {
        return gitLabProjects;
    }

    public GitlabAPI getGitlabAPI() {
        return gitlabAPI;
    }

    public StoreUpdateManager<GitlabEventMessage> getGitLabUpdateManager() {
        return gitLabUpdateManager;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public GitLabManagerData(String gitLabToken,
                             String gitLabURL,
                             String adaptorBaseURL,
                             String projectsToExposeParam) {

        this.gitLabProjects = new HashMap<>();

        this.getGitLabURL = gitLabURL;
        this.gitLabToken = gitLabToken;
        this.adaptorBaseURL = adaptorBaseURL;

        this.projectsToExpose = new HashSet<>();
        projectsToExposeParam = projectsToExposeParam.replaceAll("\\s+","");
        if (!projectsToExposeParam.equals("*")) {
            projectsToExpose.addAll(Arrays.asList(projectsToExposeParam.split(",")));
        }

        gitLabProjectExtractor = new GitLabProjectResourceExtractor(this);
        changeRequestExtractor = new ChangeRequestResourceExtractor(this);

        gitlabAPI = GitlabAPI.connect(gitLabURL, gitLabToken);
        gitlabAPI.dispatch();
    }

    public static class GitLabProjectInfos {
        public GitlabProject gitlabProject;
        public ServiceProvider serviceProvider;
        public String serviceProviderId;
        public File localRepo;
        public GitRepository gitRepo;
        //TODO: should not be parametrized with only FileResource
        public RepositoryResourceManager<FileResource> repositoryManager;
        public StoreUpdateManager<RevisionMessage> updateManager;
    }

    public void initResourceExtraction () {

        List<AbstractResource> changeRequestResources = changeRequestExtractor.extractChangeRequests_init();

        URI serviceProviderURI = ServiceProviderCatalogSingleton.constructCmServicesServiceProviderURI(GitLabConstants.CHANGE_MANAGEMENT_SERVICE_PROVIDER_ID);
        try {
            store.putResources(serviceProviderURI, changeRequestResources);
        } catch (StoreAccessException e) {
            e.printStackTrace();
        }

        List<AbstractResource> projectResources = gitLabProjectExtractor.extractGitLabProjects_init();

        serviceProviderURI = ServiceProviderCatalogSingleton.constructPmServicesServiceProviderURI(GitLabConstants.PROJECT_MANAGEMENT_SERVICE_PROVIDER_ID);
        try {
            store.putResources(serviceProviderURI, projectResources);
        } catch (StoreAccessException e) {
            e.printStackTrace();
        }

    }

    public void initializeGitlabUpdateManager () {
        GitlabChangeProviderImpl changeProvider = new GitlabChangeProviderImpl();
        gitLabUpdateManager = new StoreUpdateManager<GitlabEventMessage>(store, changeProvider);
        gitLabUpdateManager.addHandler(new GitLabEventHandler());
    }

    public void initializeUserList (String gitlabGroupStr) {

        gitLabUsers = new HashMap<>();
        try {
            this.gitlabGroup = gitlabAPI.getGroup(gitlabGroupStr);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            List<GitlabProjectMember> gitlabMembers = gitlabAPI.getNamespaceMembers(gitlabGroup.getId());
            // In the code below we instantiate Person objects, but we are not using them so far
            // For sake of simplicity, when a user has to be referenced from a resource, we simply provide
            // the Gitlab URL towards that user.
            for (GitlabProjectMember member : gitlabMembers) {
                Person person = new Person();
                person.setName(member.getName());
                person.setLogin(member.getUsername());
                URI basicPersonURI = new URI (GitLabConstants.GITLAB_PLATFORM_PREFIX_URL + member.getUsername());
                person.setAbout(basicPersonURI);

                gitLabUsers.put(member.getId().toString(), member);
                //resources.add(person);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public GitlabUser getOrRetrieveGitlabUser (String userID) {
        if (gitLabUsers.containsKey(userID)) {
            return gitLabUsers.get(userID);
        } else {
            Integer userIDInt = new Integer(userID);
            GitlabUser newUser = null;
            try {
                newUser = gitlabAPI.getUser(userIDInt);
                gitLabUsers.put(userID, newUser);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newUser;
        }
    }

    public void initializeGitLabProjects (String reposRootFolder) {
        File repoDirectory = new File(reposRootFolder);

        deleteFolder(repoDirectory);

        List<GitlabProject> projects = null;
        try {
            projects = gitlabAPI.getProjects();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (GitlabProject project : projects) {
            if (!projectsToExpose.isEmpty() && !projectsToExpose.contains(project.getName())) {
                continue;
            }

            GitLabManagerData.GitLabProjectInfos info = new GitLabManagerData.GitLabProjectInfos();
            info.gitlabProject = project;
            gitLabProjects.put(project.getName(), info);
            createGitRepositoryServiceProvider (info);

            String pathRepo = reposRootFolder + "/" + project.getName();
            File repo = new File(pathRepo);
            cloneGitRepository(project.getHttpUrl(), repo);
            info.localRepo = repo;
            try {
                info.gitRepo = new GitRepository(pathRepo);
            } catch (IOException e) {
                e.printStackTrace();
            }

            FileManagementResourceExtractor extractor = new FileManagementResourceExtractor(info.serviceProviderId);
            try {
                info.repositoryManager = new RepositoryResourceManager<>(info.gitRepo,
                        store, extractor, FileResource.class);
            } catch (RepositoryAccessException e) {
                e.printStackTrace();
            }
            RepositoryChangeProviderImpl<AbstractResource> changeProvider = new RepositoryChangeProviderImpl<>(
                    info.gitRepo, extractor, info.serviceProviderId);
            StoreUpdateManager<RevisionMessage> updateManager = new StoreUpdateManager<>(store, changeProvider);
            updateManager.addHandler(new TrsDiffHandler(GitLabConstants.TRS_GRAPH_NAME));
            updateManager.addHandler(new RevisionHandler());
            updateManager.addHandler(new ResourceVersionHandler());
            info.updateManager = updateManager;

            // We do not call this method poll since we want to update the git repo only when new push
            // events from Gitlab will be submitted to our adaptor
            //updateManager.poll(null, 1000); //TODO: set timer

            List<Revision> revList = info.gitRepo.getRevisions();
            for (Revision rev : revList) {
                RevisionMessage message = new RevisionMessage(info.serviceProviderId, rev);
                ZonedDateTime zd = rev.getTimestamp();
                Optional<RevisionMessage> m = Optional.of(message);
                updateManager.submit(zd, m);
            }

            LOGGER.info("GitLab project '{}' initialized.", project.getName());
        }
    }

    private static void deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if(files!=null) { //some JVMs return null for empty dirs
            for(File f: files) {
                if(f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
        }
        folder.delete();
    }

    private void createGitRepositoryServiceProvider (GitLabManagerData.GitLabProjectInfos info) {
        String title = info.gitlabProject.getName();
        // TODO: update later on with the description coming from GitLab
        String description = "Content of the Git repository " + title + ".";
        Publisher publisher = null;
        Map<String,Object> parameterValueMap = new HashMap<>();
        //String serviceProviderId = GitLabConstants.FILE_MANAGEMENT_SERVICE_PROVIDER_ID
        //        + "-"
        //        + info.gitlabProject.getName();
        //Integer projectID = new Integer(info.gitlabProject.getId());
        //String serviceProviderId = projectID.toString();
        String serviceProviderId = info.gitlabProject.getName();
        parameterValueMap.put("serviceProviderId", serviceProviderId);

        ServiceProvider serviceProvider = null;
        try {
            serviceProvider = FmServicesServiceProvidersFactory.createServiceProvider(adaptorBaseURL,
                    title,
                    description,
                    publisher,
                    parameterValueMap);
        } catch (OslcCoreApplicationException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        info.serviceProvider = serviceProvider;
        info.serviceProviderId = serviceProviderId;
    }

    private void cloneGitRepository (String uri, File localRepo) {
        SshSessionFactory sshSessionFactory = new JschConfigSessionFactory() {
            @Override
            protected void configure(OpenSshConfig.Host host, Session session ) {
                // do nothing
            }
        };
        CloneCommand cloneCommand = Git.cloneRepository();
        cloneCommand.setURI( "git@gitlab.com:" + uri.substring(19));
        cloneCommand.setDirectory(localRepo);
        cloneCommand.setTransportConfigCallback( new TransportConfigCallback() {
            @Override
            public void configure( Transport transport ) {
                SshTransport sshTransport = ( SshTransport )transport;
                sshTransport.setSshSessionFactory( sshSessionFactory );
            }
        } );
        try {
            cloneCommand.call();
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
    }

    public void pullGitRepository (File localRepo) {
        SshSessionFactory sshSessionFactory = new JschConfigSessionFactory() {
            @Override
            protected void configure(OpenSshConfig.Host host, Session session ) {
                // do nothing
            }
        };

        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        Repository repository = null;
        try {
            repository = builder.setWorkTree(localRepo).readEnvironment().build();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Git git = new Git(repository);

        PullCommand pullCommand = git.pull();
        pullCommand.setTransportConfigCallback( new TransportConfigCallback() {
            @Override
            public void configure( Transport transport ) {
                SshTransport sshTransport = ( SshTransport )transport;
                sshTransport.setSshSessionFactory( sshSessionFactory );
            }
        } );
        try {
            pullCommand.call();
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
    }

}
