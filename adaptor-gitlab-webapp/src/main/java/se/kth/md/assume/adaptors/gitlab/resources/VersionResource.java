/*******************************************************************************
 * Copyright (c) 2012 IBM Corporation and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v. 1.0 which accompanies this distribution.
 *
 * The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *
 *     Russell Boykin       - initial API and implementation
 *     Alberto Giammaria    - initial API and implementation
 *     Chris Peters         - initial API and implementation
 *     Gianluca Bernardini  - initial API and implementation
 *	   Sam Padgett          - initial API and implementation
 *     Michael Fiedler      - adapted for OSLC4J
 *     Jad El-khoury        - initial implementation of code generator (https://bugs.eclipse.org/bugs/show_bug.cgi?id=422448)
 *     Matthieu Helleboid   - Support for multiple Service Providers.
 *     Anass Radouani       - Support for multiple Service Providers.
 *
 * This file is generated by org.eclipse.lyo.oslc4j.codegenerator
 *******************************************************************************/

package se.kth.md.assume.adaptors.gitlab.resources;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import javax.ws.rs.core.UriBuilder;

import org.eclipse.lyo.oslc4j.core.annotation.OslcAllowedValue;
import org.eclipse.lyo.oslc4j.core.annotation.OslcDescription;
import org.eclipse.lyo.oslc4j.core.annotation.OslcMemberProperty;
import org.eclipse.lyo.oslc4j.core.annotation.OslcName;
import org.eclipse.lyo.oslc4j.core.annotation.OslcNamespace;
import org.eclipse.lyo.oslc4j.core.annotation.OslcOccurs;
import org.eclipse.lyo.oslc4j.core.annotation.OslcPropertyDefinition;
import org.eclipse.lyo.oslc4j.core.annotation.OslcRange;
import org.eclipse.lyo.oslc4j.core.annotation.OslcReadOnly;
import org.eclipse.lyo.oslc4j.core.annotation.OslcRepresentation;
import org.eclipse.lyo.oslc4j.core.annotation.OslcResourceShape;
import org.eclipse.lyo.oslc4j.core.annotation.OslcTitle;
import org.eclipse.lyo.oslc4j.core.annotation.OslcValueType;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.Occurs;
import org.eclipse.lyo.oslc4j.core.model.OslcConstants;
import org.eclipse.lyo.oslc4j.core.model.Representation;
import org.eclipse.lyo.oslc4j.core.model.ValueType;

import se.kth.md.assume.adaptors.gitlab.servlet.ServletListener;
import se.kth.md.assume.adaptors.gitlab.GitLabConstants;

// Start of user code imports
// End of user code

// Start of user code preClassCode
// End of user code

// Start of user code classAnnotations
// End of user code
@OslcNamespace(GitLabConstants.CFGM_NAMSPACE)
@OslcName(GitLabConstants.VERSIONRESOURCE)
@OslcResourceShape(title = "VersionResource Resource Shape", describes = GitLabConstants.TYPE_VERSIONRESOURCE)
public class VersionResource
    extends AbstractResource
    implements IVersionResource
{
    // Start of user code attributeAnnotation:isVersionOf
    // End of user code
    private Link isVersionOf = new Link();
    // Start of user code attributeAnnotation:versionId
    // End of user code
    private String versionId;
    // Start of user code attributeAnnotation:created
    // End of user code
    private Date created;
    // Start of user code attributeAnnotation:modfied
    // End of user code
    private Date modfied;
    // Start of user code attributeAnnotation:subject
    // End of user code
    private HashSet<String> subject = new HashSet<String>();
    // Start of user code attributeAnnotation:identifier
    // End of user code
    private String identifier;
    
    // Start of user code classAttributes
    // End of user code
    // Start of user code classMethods
    // End of user code
    public VersionResource()
           throws URISyntaxException
    {
        super();
    
        // Start of user code constructor1
        // End of user code
    }
    
    public VersionResource(final URI about)
           throws URISyntaxException
    {
        super(about);
    
        // Start of user code constructor2
        // End of user code
    }
    
    public VersionResource(final String serviceProviderId, final String resourceId)
           throws URISyntaxException
    {
        this (constructURI(serviceProviderId, resourceId));
        // Start of user code constructor3
        // End of user code
    }
    
    public static URI constructURI(final String serviceProviderId, final String resourceId)
    {
        String basePath = ServletListener.getServicesBase();
        Map<String, Object> pathParameters = new HashMap<String, Object>();
        pathParameters.put("serviceProviderId", serviceProviderId);
        pathParameters.put("resourceId", resourceId);
        String instanceURI = "fm-services/{serviceProviderId}/resources/versionResources/{resourceId}";
    
        final UriBuilder builder = UriBuilder.fromUri(basePath);
        return builder.path(instanceURI).buildFromMap(pathParameters);
    }
    
    public static Link constructLink(final String serviceProviderId, final String resourceId , final String label)
    {
        return new Link(constructURI(serviceProviderId, resourceId), label);
    }
    
    public static Link constructLink(final String serviceProviderId, final String resourceId)
    {
        return new Link(constructURI(serviceProviderId, resourceId));
    }
    
    public String toString()
    {
        return toString(false);
    }
    
    public String toString(boolean asLocalResource)
    {
        String result = "";
        // Start of user code toString_init
        // End of user code
    
        if (asLocalResource) {
            result = result + "{a Local VersionResource Resource} - update VersionResource.toString() to present resource as desired.";
            // Start of user code toString_bodyForLocalResource
            // End of user code
        }
        else {
            result = getAbout().toString();
        }
    
        // Start of user code toString_finalize
        // End of user code
    
        return result;
    }
    
    public String toHtml()
    {
        return toHtml(false);
    }
    
    public String toHtml(boolean asLocalResource)
    {
        String result = "";
        // Start of user code toHtml_init
        // End of user code
    
        if (asLocalResource) {
            result = toString(true);
            // Start of user code toHtml_bodyForLocalResource
            // End of user code
        }
        else {
            result = "<a href=\"" + getAbout() + "\">" + toString() + "</a>";
        }
    
        // Start of user code toHtml_finalize
        // End of user code
    
        return result;
    }
    
    public void addSubject(final String subject)
    {
        this.subject.add(subject);
    }
    
    
    // Start of user code getterAnnotation:isVersionOf
    // End of user code
    @OslcName("isVersionOf")
    @OslcPropertyDefinition(GitLabConstants.DUBLIN_CORE_NAMSPACE + "isVersionOf")
    @OslcOccurs(Occurs.ExactlyOne)
    @OslcValueType(ValueType.Resource)
    @OslcReadOnly(false)
    public Link getIsVersionOf()
    {
        // Start of user code getterInit:isVersionOf
        // End of user code
        return isVersionOf;
    }
    
    // Start of user code getterAnnotation:versionId
    // End of user code
    @OslcName("versionId")
    @OslcPropertyDefinition(GitLabConstants.CFGM_NAMSPACE + "versionId")
    @OslcOccurs(Occurs.ExactlyOne)
    @OslcValueType(ValueType.String)
    @OslcReadOnly(false)
    public String getVersionId()
    {
        // Start of user code getterInit:versionId
        // End of user code
        return versionId;
    }
    
    // Start of user code getterAnnotation:created
    // End of user code
    @OslcName("created")
    @OslcPropertyDefinition(GitLabConstants.DUBLIN_CORE_NAMSPACE + "created")
    @OslcOccurs(Occurs.ZeroOrOne)
    @OslcValueType(ValueType.DateTime)
    @OslcReadOnly(false)
    @OslcTitle("")
    public Date getCreated()
    {
        // Start of user code getterInit:created
        // End of user code
        return created;
    }
    
    // Start of user code getterAnnotation:modfied
    // End of user code
    @OslcName("modfied")
    @OslcPropertyDefinition(GitLabConstants.DUBLIN_CORE_NAMSPACE + "modfied")
    @OslcOccurs(Occurs.ZeroOrOne)
    @OslcValueType(ValueType.DateTime)
    @OslcReadOnly(false)
    @OslcTitle("")
    public Date getModfied()
    {
        // Start of user code getterInit:modfied
        // End of user code
        return modfied;
    }
    
    // Start of user code getterAnnotation:subject
    // End of user code
    @OslcName("subject")
    @OslcPropertyDefinition(GitLabConstants.DUBLIN_CORE_NAMSPACE + "subject")
    @OslcOccurs(Occurs.ZeroOrMany)
    @OslcValueType(ValueType.String)
    @OslcReadOnly(false)
    public HashSet<String> getSubject()
    {
        // Start of user code getterInit:subject
        // End of user code
        return subject;
    }
    
    // Start of user code getterAnnotation:identifier
    // End of user code
    @OslcName("identifier")
    @OslcPropertyDefinition(GitLabConstants.DUBLIN_CORE_NAMSPACE + "identifier")
    @OslcOccurs(Occurs.ZeroOrOne)
    @OslcValueType(ValueType.String)
    @OslcReadOnly(false)
    public String getIdentifier()
    {
        // Start of user code getterInit:identifier
        // End of user code
        return identifier;
    }
    
    
    // Start of user code setterAnnotation:isVersionOf
    // End of user code
    public void setIsVersionOf(final Link isVersionOf )
    {
        // Start of user code setterInit:isVersionOf
        // End of user code
        this.isVersionOf = isVersionOf;
    
        // Start of user code setterFinalize:isVersionOf
        // End of user code
    }
    
    // Start of user code setterAnnotation:versionId
    // End of user code
    public void setVersionId(final String versionId )
    {
        // Start of user code setterInit:versionId
        // End of user code
        this.versionId = versionId;
    
        // Start of user code setterFinalize:versionId
        // End of user code
    }
    
    // Start of user code setterAnnotation:created
    // End of user code
    public void setCreated(final Date created )
    {
        // Start of user code setterInit:created
        // End of user code
        this.created = created;
    
        // Start of user code setterFinalize:created
        // End of user code
    }
    
    // Start of user code setterAnnotation:modfied
    // End of user code
    public void setModfied(final Date modfied )
    {
        // Start of user code setterInit:modfied
        // End of user code
        this.modfied = modfied;
    
        // Start of user code setterFinalize:modfied
        // End of user code
    }
    
    // Start of user code setterAnnotation:subject
    // End of user code
    public void setSubject(final HashSet<String> subject )
    {
        // Start of user code setterInit:subject
        // End of user code
        this.subject.clear();
        if (subject != null)
        {
            this.subject.addAll(subject);
        }
    
        // Start of user code setterFinalize:subject
        // End of user code
    }
    
    // Start of user code setterAnnotation:identifier
    // End of user code
    public void setIdentifier(final String identifier )
    {
        // Start of user code setterInit:identifier
        // End of user code
        this.identifier = identifier;
    
        // Start of user code setterFinalize:identifier
        // End of user code
    }
    
    
    static public String isVersionOfToHtmlForCreation (final HttpServletRequest httpServletRequest)
    {
        String s = "";
    
        // Start of user code "Init:isVersionOfToHtmlForCreation(...)"
        // End of user code
    
        s = s + "<label for=\"isVersionOf\">isVersionOf: </LABEL>";
    
        // Start of user code "Mid:isVersionOfToHtmlForCreation(...)"
        // End of user code
    
        // Start of user code "Finalize:isVersionOfToHtmlForCreation(...)"
        // End of user code
    
        return s;
    }
    
    static public String versionIdToHtmlForCreation (final HttpServletRequest httpServletRequest)
    {
        String s = "";
    
        // Start of user code "Init:versionIdToHtmlForCreation(...)"
        // End of user code
    
        s = s + "<label for=\"versionId\">versionId: </LABEL>";
    
        // Start of user code "Mid:versionIdToHtmlForCreation(...)"
        // End of user code
    
        s= s + "<input name=\"versionId\" type=\"text\" style=\"width: 400px\" id=\"versionId\" >";
        // Start of user code "Finalize:versionIdToHtmlForCreation(...)"
        // End of user code
    
        return s;
    }
    
    static public String createdToHtmlForCreation (final HttpServletRequest httpServletRequest)
    {
        String s = "";
    
        // Start of user code "Init:createdToHtmlForCreation(...)"
        // End of user code
    
        s = s + "<label for=\"created\">created: </LABEL>";
    
        // Start of user code "Mid:createdToHtmlForCreation(...)"
        // End of user code
    
        s= s + "<input name=\"created\" type=\"text\" style=\"width: 400px\" id=\"created\" >";
        // Start of user code "Finalize:createdToHtmlForCreation(...)"
        // End of user code
    
        return s;
    }
    
    static public String modfiedToHtmlForCreation (final HttpServletRequest httpServletRequest)
    {
        String s = "";
    
        // Start of user code "Init:modfiedToHtmlForCreation(...)"
        // End of user code
    
        s = s + "<label for=\"modfied\">modfied: </LABEL>";
    
        // Start of user code "Mid:modfiedToHtmlForCreation(...)"
        // End of user code
    
        s= s + "<input name=\"modfied\" type=\"text\" style=\"width: 400px\" id=\"modfied\" >";
        // Start of user code "Finalize:modfiedToHtmlForCreation(...)"
        // End of user code
    
        return s;
    }
    
    static public String subjectToHtmlForCreation (final HttpServletRequest httpServletRequest)
    {
        String s = "";
    
        // Start of user code "Init:subjectToHtmlForCreation(...)"
        // End of user code
    
        s = s + "<label for=\"subject\">subject: </LABEL>";
    
        // Start of user code "Mid:subjectToHtmlForCreation(...)"
        // End of user code
    
        s= s + "<input name=\"subject\" type=\"text\" style=\"width: 400px\" id=\"subject\" >";
        // Start of user code "Finalize:subjectToHtmlForCreation(...)"
        // End of user code
    
        return s;
    }
    
    static public String identifierToHtmlForCreation (final HttpServletRequest httpServletRequest)
    {
        String s = "";
    
        // Start of user code "Init:identifierToHtmlForCreation(...)"
        // End of user code
    
        s = s + "<label for=\"identifier\">identifier: </LABEL>";
    
        // Start of user code "Mid:identifierToHtmlForCreation(...)"
        // End of user code
    
        s= s + "<input name=\"identifier\" type=\"text\" style=\"width: 400px\" id=\"identifier\" >";
        // Start of user code "Finalize:identifierToHtmlForCreation(...)"
        // End of user code
    
        return s;
    }
    
    
    public String isVersionOfToHtml()
    {
        String s = "";
    
        // Start of user code isVersionOftoHtml_init
        // End of user code
    
        s = s + "<label for=\"isVersionOf\"><strong>isVersionOf</strong>: </LABEL>";
    
        // Start of user code isVersionOftoHtml_mid
        // End of user code
    
        try {
            if (isVersionOf.getValue() == null) {
                s = s + "<em>null</em>";
            }
            else {
                s = s + isVersionOf.getValue().toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    
        // Start of user code isVersionOftoHtml_finalize
        // End of user code
    
        return s;
    }
    
    public String versionIdToHtml()
    {
        String s = "";
    
        // Start of user code versionIdtoHtml_init
        // End of user code
    
        s = s + "<label for=\"versionId\"><strong>versionId</strong>: </LABEL>";
    
        // Start of user code versionIdtoHtml_mid
        // End of user code
    
        try {
            if (versionId == null) {
                s= s + "<em>null</em>";
            }
            else {
                s= s + versionId.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    
        // Start of user code versionIdtoHtml_finalize
        // End of user code
    
        return s;
    }
    
    public String createdToHtml()
    {
        String s = "";
    
        // Start of user code createdtoHtml_init
        // End of user code
    
        s = s + "<label for=\"created\"><strong>created</strong>: </LABEL>";
    
        // Start of user code createdtoHtml_mid
        // End of user code
    
        try {
            if (created == null) {
                s= s + "<em>null</em>";
            }
            else {
                s= s + created.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    
        // Start of user code createdtoHtml_finalize
        // End of user code
    
        return s;
    }
    
    public String modfiedToHtml()
    {
        String s = "";
    
        // Start of user code modfiedtoHtml_init
        // End of user code
    
        s = s + "<label for=\"modfied\"><strong>modfied</strong>: </LABEL>";
    
        // Start of user code modfiedtoHtml_mid
        // End of user code
    
        try {
            if (modfied == null) {
                s= s + "<em>null</em>";
            }
            else {
                s= s + modfied.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    
        // Start of user code modfiedtoHtml_finalize
        // End of user code
    
        return s;
    }
    
    public String subjectToHtml()
    {
        String s = "";
    
        // Start of user code subjecttoHtml_init
        // End of user code
    
        s = s + "<label for=\"subject\"><strong>subject</strong>: </LABEL>";
    
        // Start of user code subjecttoHtml_mid
        // End of user code
    
        try {
            s = s + "<ul>";
            Iterator<String> itr = subject.iterator();
            while(itr.hasNext()) {
                s = s + "<li>";
                s= s + itr.next().toString();
                s = s + "</li>";
            }
            s = s + "</ul>";
        } catch (Exception e) {
            e.printStackTrace();
        }
    
        // Start of user code subjecttoHtml_finalize
        // End of user code
    
        return s;
    }
    
    public String identifierToHtml()
    {
        String s = "";
    
        // Start of user code identifiertoHtml_init
        // End of user code
    
        s = s + "<label for=\"identifier\"><strong>identifier</strong>: </LABEL>";
    
        // Start of user code identifiertoHtml_mid
        // End of user code
    
        try {
            if (identifier == null) {
                s= s + "<em>null</em>";
            }
            else {
                s= s + identifier.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    
        // Start of user code identifiertoHtml_finalize
        // End of user code
    
        return s;
    }
    
    
}
