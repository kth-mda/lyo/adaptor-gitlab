package se.kth.md.assume.adaptors.gitlab.updates;

import org.eclipse.lyo.tools.store.update.OSLCMessage;
import org.gitlab.api.models.GitlabIssue;
import se.kth.md.assume.adaptors.gitlab.GitLabManagerData;

/**
 * Created by Frederic Loiret on 2017-02-21.
 */
// TODO: Split up into 2 classes, one for PUSH events, one for ISSUE ones.
public class GitlabEventMessage extends OSLCMessage {
    public enum GITLAB_EVENT {
        PUSH,
        ISSUE
    }

    public enum ISSUE_STATUS {
        ISSUE_CREATED,
        ISSUE_UPDATED,
        ISSUE_CLOSED,
        ISSUE_REOPENED
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    private String projectName;

    public GITLAB_EVENT getEvent() {
        return event;
    }

    private GITLAB_EVENT event;

    public ISSUE_STATUS getIssue_status() {
        return issue_status;
    }

    public void setIssue_status(ISSUE_STATUS issue_status) {
        this.issue_status = issue_status;
    }

    private ISSUE_STATUS issue_status;

    public GitLabManagerData getGitLabManagerData() {
        return gitLabManagerData;
    }

    public void setGitLabManagerData(GitLabManagerData gitLabManagerData) {
        this.gitLabManagerData = gitLabManagerData;
    }

    private GitLabManagerData gitLabManagerData;

    private String projectID;

    public GitlabIssue getIssue() {
        return issue;
    }

    public void setIssue(GitlabIssue issue) {
        this.issue = issue;
    }

    private GitlabIssue issue;

    public String getProjectID() {
        return projectID;
    }

    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    public String getIssueID() {
        return issueID;
    }

    public void setIssueID(String issueID) {
        this.issueID = issueID;
    }

    private String issueID;

    // Do I need the serviceProviderId here?
    public GitlabEventMessage (String serviceProviderId,
                               GITLAB_EVENT event,
                               GitLabManagerData gitLabManagerData) {
        super(serviceProviderId);
        this.event = event;
        this.gitLabManagerData = gitLabManagerData;
    }

    @Override
    public String toString() {
        String e = "";
        if (event.equals(GITLAB_EVENT.PUSH.PUSH)) {
            e = "PUSH";
        } else if (event.equals(GITLAB_EVENT.ISSUE.ISSUE)) {
            e = "ISSUE";
        }
        return "Gitlab Event: " + e;
    }
}
