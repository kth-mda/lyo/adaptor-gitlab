package se.kth.md.assume.adaptors.gitlab.extractors;

import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.gitlab.api.models.GitlabIssue;
import se.kth.md.assume.adaptors.gitlab.GitLabConstants;
import se.kth.md.assume.adaptors.gitlab.GitLabManagerData;
import se.kth.md.assume.adaptors.gitlab.resources.ChangeRequest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ChangeRequestResourceExtractor {

    private GitLabManagerData gitLabManagerData;

    public ChangeRequestResourceExtractor(GitLabManagerData gitLabManagerData) {
        this.gitLabManagerData = gitLabManagerData;
    }

    public List<AbstractResource> extractChangeRequests_init () {
        List<AbstractResource> resources = new ArrayList<>();

        for (GitLabManagerData.GitLabProjectInfos info : gitLabManagerData.getGitLabProjects().values()) {
            try {
                List<GitlabIssue> issueList = gitLabManagerData.getGitlabAPI().getIssues(info.gitlabProject);
                for (GitlabIssue issue : issueList) {
                    ChangeRequest cR = ResourceExtractorHelper.createNewChangeRequest(GitLabConstants.CHANGE_MANAGEMENT_SERVICE_PROVIDER_ID,
                            issue.getId(), issue.getTitle(), issue.getDescription(), issue.getCreatedAt());

                    ResourceExtractorHelper.createAndSetCreatorURI(cR, issue.getAuthor().getUsername());

                    resources.add(cR);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return resources;
    }

    public List<AbstractResource> extractChangeRequests_update () {
        List<AbstractResource> resources = new ArrayList<>();

        return resources;
    }

}
