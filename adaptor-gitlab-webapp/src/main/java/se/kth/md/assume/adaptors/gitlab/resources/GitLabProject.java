/*******************************************************************************
 * Copyright (c) 2012 IBM Corporation and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v. 1.0 which accompanies this distribution.
 *
 * The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *
 *     Russell Boykin       - initial API and implementation
 *     Alberto Giammaria    - initial API and implementation
 *     Chris Peters         - initial API and implementation
 *     Gianluca Bernardini  - initial API and implementation
 *	   Sam Padgett          - initial API and implementation
 *     Michael Fiedler      - adapted for OSLC4J
 *     Jad El-khoury        - initial implementation of code generator (https://bugs.eclipse.org/bugs/show_bug.cgi?id=422448)
 *     Matthieu Helleboid   - Support for multiple Service Providers.
 *     Anass Radouani       - Support for multiple Service Providers.
 *
 * This file is generated by org.eclipse.lyo.oslc4j.codegenerator
 *******************************************************************************/

package se.kth.md.assume.adaptors.gitlab.resources;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import javax.ws.rs.core.UriBuilder;

import org.eclipse.lyo.oslc4j.core.annotation.OslcAllowedValue;
import org.eclipse.lyo.oslc4j.core.annotation.OslcDescription;
import org.eclipse.lyo.oslc4j.core.annotation.OslcMemberProperty;
import org.eclipse.lyo.oslc4j.core.annotation.OslcName;
import org.eclipse.lyo.oslc4j.core.annotation.OslcNamespace;
import org.eclipse.lyo.oslc4j.core.annotation.OslcOccurs;
import org.eclipse.lyo.oslc4j.core.annotation.OslcPropertyDefinition;
import org.eclipse.lyo.oslc4j.core.annotation.OslcRange;
import org.eclipse.lyo.oslc4j.core.annotation.OslcReadOnly;
import org.eclipse.lyo.oslc4j.core.annotation.OslcRepresentation;
import org.eclipse.lyo.oslc4j.core.annotation.OslcResourceShape;
import org.eclipse.lyo.oslc4j.core.annotation.OslcTitle;
import org.eclipse.lyo.oslc4j.core.annotation.OslcValueType;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.Occurs;
import org.eclipse.lyo.oslc4j.core.model.OslcConstants;
import org.eclipse.lyo.oslc4j.core.model.Representation;
import org.eclipse.lyo.oslc4j.core.model.ValueType;

import se.kth.md.assume.adaptors.gitlab.servlet.ServletListener;
import se.kth.md.assume.adaptors.gitlab.GitLabConstants;
import se.kth.md.assume.adaptors.gitlab.resources.FileResource;
import se.kth.md.assume.adaptors.gitlab.resources.FolderResource;
import se.kth.md.assume.adaptors.gitlab.resources.ChangeRequest;
import se.kth.md.assume.adaptors.gitlab.resources.Person;

// Start of user code imports
// End of user code

// Start of user code preClassCode
// End of user code

// Start of user code classAnnotations
// End of user code
@OslcNamespace(GitLabConstants.PROJECT_MANAGEMENT_NAMSPACE)
@OslcName(GitLabConstants.GITLABPROJECT)
@OslcResourceShape(title = "GitLabProject Resource Shape", describes = GitLabConstants.TYPE_GITLABPROJECT)
public class GitLabProject
    extends Project
    implements IGitLabProject, IGitLabRepository
{
    // Start of user code attributeAnnotation:webURL
    // End of user code
    private URI webURL;
    // Start of user code attributeAnnotation:gitURL
    // End of user code
    private URI gitURL;
    // Start of user code attributeAnnotation:contains
    // End of user code
    private HashSet<Link> contains = new HashSet<Link>();
    
    // Start of user code classAttributes
    // End of user code
    // Start of user code classMethods
    // End of user code
    public GitLabProject()
           throws URISyntaxException
    {
        super();
    
        // Start of user code constructor1
        // End of user code
    }
    
    public GitLabProject(final URI about)
           throws URISyntaxException
    {
        super(about);
    
        // Start of user code constructor2
        // End of user code
    }
    
    public GitLabProject(final String serviceProviderId, final String projectName)
           throws URISyntaxException
    {
        this (constructURI(serviceProviderId, projectName));
        // Start of user code constructor3
        // End of user code
    }
    
    public static URI constructURI(final String serviceProviderId, final String projectName)
    {
        String basePath = ServletListener.getServicesBase();
        Map<String, Object> pathParameters = new HashMap<String, Object>();
        pathParameters.put("serviceProviderId", serviceProviderId);
        pathParameters.put("projectName", projectName);
        String instanceURI = "pm-services/{serviceProviderId}/gitLabProjects/{projectName}";
    
        final UriBuilder builder = UriBuilder.fromUri(basePath);
        return builder.path(instanceURI).buildFromMap(pathParameters);
    }
    
    public static Link constructLink(final String serviceProviderId, final String projectName , final String label)
    {
        return new Link(constructURI(serviceProviderId, projectName), label);
    }
    
    public static Link constructLink(final String serviceProviderId, final String projectName)
    {
        return new Link(constructURI(serviceProviderId, projectName));
    }
    
    public String toString()
    {
        return toString(false);
    }
    
    public String toString(boolean asLocalResource)
    {
        String result = "";
        // Start of user code toString_init
        // End of user code
    
        if (asLocalResource) {
            result = result + "{a Local GitLabProject Resource} - update GitLabProject.toString() to present resource as desired.";
            // Start of user code toString_bodyForLocalResource
            // End of user code
        }
        else {
            result = getAbout().toString();
        }
    
        // Start of user code toString_finalize
        // End of user code
    
        return result;
    }
    
    public String toHtml()
    {
        return toHtml(false);
    }
    
    public String toHtml(boolean asLocalResource)
    {
        String result = "";
        // Start of user code toHtml_init
        // End of user code
    
        if (asLocalResource) {
            result = toString(true);
            // Start of user code toHtml_bodyForLocalResource
            // End of user code
        }
        else {
            result = "<a href=\"" + getAbout() + "\">" + toString() + "</a>";
        }
    
        // Start of user code toHtml_finalize
        // End of user code
    
        return result;
    }
    
    public void addContains(final Link contains)
    {
        this.contains.add(contains);
    }
    
    
    // Start of user code getterAnnotation:webURL
    // End of user code
    @OslcName("webURL")
    @OslcPropertyDefinition(GitLabConstants.PROJECT_MANAGEMENT_NAMSPACE + "webURL")
    @OslcOccurs(Occurs.ZeroOrOne)
    @OslcReadOnly(true)
    public URI getWebURL()
    {
        // Start of user code getterInit:webURL
        // End of user code
        return webURL;
    }
    
    // Start of user code getterAnnotation:gitURL
    // End of user code
    @OslcName("gitURL")
    @OslcPropertyDefinition(GitLabConstants.PROJECT_MANAGEMENT_NAMSPACE + "gitURL")
    @OslcOccurs(Occurs.ZeroOrOne)
    @OslcReadOnly(true)
    public URI getGitURL()
    {
        // Start of user code getterInit:gitURL
        // End of user code
        return gitURL;
    }
    
    // Start of user code getterAnnotation:contains
    // End of user code
    @OslcName("contains")
    @OslcPropertyDefinition(GitLabConstants.PROJECT_MANAGEMENT_NAMSPACE + "contains")
    @OslcOccurs(Occurs.ZeroOrMany)
    @OslcValueType(ValueType.Resource)
    @OslcRange({GitLabConstants.TYPE_FILERESOURCE, GitLabConstants.TYPE_FOLDERRESOURCE})
    @OslcReadOnly(false)
    public HashSet<Link> getContains()
    {
        // Start of user code getterInit:contains
        // End of user code
        return contains;
    }
    
    
    // Start of user code setterAnnotation:webURL
    // End of user code
    public void setWebURL(final URI webURL )
    {
        // Start of user code setterInit:webURL
        // End of user code
        this.webURL = webURL;
    
        // Start of user code setterFinalize:webURL
        // End of user code
    }
    
    // Start of user code setterAnnotation:gitURL
    // End of user code
    public void setGitURL(final URI gitURL )
    {
        // Start of user code setterInit:gitURL
        // End of user code
        this.gitURL = gitURL;
    
        // Start of user code setterFinalize:gitURL
        // End of user code
    }
    
    // Start of user code setterAnnotation:contains
    // End of user code
    public void setContains(final HashSet<Link> contains )
    {
        // Start of user code setterInit:contains
        // End of user code
        this.contains.clear();
        if (contains != null)
        {
            this.contains.addAll(contains);
        }
    
        // Start of user code setterFinalize:contains
        // End of user code
    }
    
    
    static public String webURLToHtmlForCreation (final HttpServletRequest httpServletRequest)
    {
        String s = "";
    
        // Start of user code "Init:webURLToHtmlForCreation(...)"
        // End of user code
    
        s = s + "<label for=\"webURL\">webURL: </LABEL>";
    
        // Start of user code "Mid:webURLToHtmlForCreation(...)"
        // End of user code
    
        s= s + "<input name=\"webURL\" type=\"text\" style=\"width: 400px\" id=\"webURL\" >";
        // Start of user code "Finalize:webURLToHtmlForCreation(...)"
        // End of user code
    
        return s;
    }
    
    static public String gitURLToHtmlForCreation (final HttpServletRequest httpServletRequest)
    {
        String s = "";
    
        // Start of user code "Init:gitURLToHtmlForCreation(...)"
        // End of user code
    
        s = s + "<label for=\"gitURL\">gitURL: </LABEL>";
    
        // Start of user code "Mid:gitURLToHtmlForCreation(...)"
        // End of user code
    
        s= s + "<input name=\"gitURL\" type=\"text\" style=\"width: 400px\" id=\"gitURL\" >";
        // Start of user code "Finalize:gitURLToHtmlForCreation(...)"
        // End of user code
    
        return s;
    }
    
    static public String containsToHtmlForCreation (final HttpServletRequest httpServletRequest)
    {
        String s = "";
    
        // Start of user code "Init:containsToHtmlForCreation(...)"
        // End of user code
    
        s = s + "<label for=\"contains\">contains: </LABEL>";
    
        // Start of user code "Mid:containsToHtmlForCreation(...)"
        // End of user code
    
        // Start of user code "Finalize:containsToHtmlForCreation(...)"
        // End of user code
    
        return s;
    }
    
    
    public String webURLToHtml()
    {
        String s = "";
    
        // Start of user code webURLtoHtml_init
        // End of user code
    
        s = s + "<label for=\"webURL\"><strong>webURL</strong>: </LABEL>";
    
        // Start of user code webURLtoHtml_mid
        // End of user code
    
        try {
            if (webURL == null) {
                s= s + "<em>null</em>";
            }
            else {
                s= s + webURL.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    
        // Start of user code webURLtoHtml_finalize
        s = JSPHelper.createHTTPLinks(s);
        // End of user code
    
        return s;
    }
    
    public String gitURLToHtml()
    {
        String s = "";
    
        // Start of user code gitURLtoHtml_init
        // End of user code
    
        s = s + "<label for=\"gitURL\"><strong>gitURL</strong>: </LABEL>";
    
        // Start of user code gitURLtoHtml_mid
        // End of user code
    
        try {
            if (gitURL == null) {
                s= s + "<em>null</em>";
            }
            else {
                s= s + gitURL.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    
        // Start of user code gitURLtoHtml_finalize
        s = JSPHelper.createHTTPLinks(s);
        // End of user code
    
        return s;
    }
    
    public String containsToHtml()
    {
        String s = "";
    
        // Start of user code containstoHtml_init
        // End of user code
    
        s = s + "<label for=\"contains\"><strong>contains</strong>: </LABEL>";
    
        // Start of user code containstoHtml_mid
        // End of user code
    
        try {
            s = s + "<ul>";
            for(Link next : contains) {
                s = s + "<li>";
                if (next.getValue() == null) {
                    s= s + "<em>null</em>";
                }
                else {
                    s = s + next.getValue().toString();
                }
                s = s + "</li>";
            }
            s = s + "</ul>";
        } catch (Exception e) {
            e.printStackTrace();
        }
    
        // Start of user code containstoHtml_finalize
        // End of user code
    
        return s;
    }
    
    
}
