package se.kth.md.assume.adaptors.gitlab.extractors;

import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import se.kth.md.aide.diff.RepositoryResourceExtractor;
import se.kth.md.aide.repository.api.RevisionedObject;
import se.kth.md.assume.adaptors.gitlab.resources.FileResource;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class FileManagementResourceExtractor implements RepositoryResourceExtractor<AbstractResource> {

    private String serviceProviderID;

    // TODO: refactor the whole code to accept a list of suffix instead
    private String fileSuffix = ".c";

    public FileManagementResourceExtractor(String serviceProviderID) {
        this.serviceProviderID = serviceProviderID;
    }

    @Override
    public String getFileSuffix() {
        return fileSuffix;
    }

    @Override
    public List<AbstractResource> extractFileRevisionResources(RevisionedObject revisionedObject, InputStream objectStream) {

        List<AbstractResource> list = new ArrayList<>();

        String path = revisionedObject.getPath();

        String revId = revisionedObject.getRevision().getId();

        // TODO: change the way resource IDs are constructed
        // TODO: figure out how the service provider IDs should be managed
        Integer id = new Integer(path.hashCode());
        FileResource fr = ResourceExtractorHelper.createNewFileResource(serviceProviderID,
                id.toString(), getFileNameFromPath(path), revId, null);
        list.add(fr);

        return list;
    }

    private String getFileNameFromPath (String path) {
        if (!path.contains("/")) {
            return path;
        } else {
            String[] paths = path.split("/");
            return paths[paths.length - 1];
        }
    }
}
