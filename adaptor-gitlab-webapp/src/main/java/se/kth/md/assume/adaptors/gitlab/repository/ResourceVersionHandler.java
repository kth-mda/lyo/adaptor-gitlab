package se.kth.md.assume.adaptors.gitlab.repository;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.NullArgumentException;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.tools.store.Store;
import org.eclipse.lyo.tools.store.StoreAccessException;
import org.eclipse.lyo.tools.store.update.Handler;
import org.eclipse.lyo.tools.store.update.change.Change;
import org.eclipse.lyo.tools.store.update.change.ChangeHelper;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.assume.adaptors.gitlab.GitLabConstants;
import se.kth.md.assume.adaptors.gitlab.resources.VersionResource;
import se.kth.md.assume.repository.update.RevisionMessage;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * ResourceVersionHandler is .
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-09-19
 */
public class ResourceVersionHandler implements Handler<RevisionMessage> {
    private final static Logger LOGGER = LoggerFactory.getLogger(ResourceVersionHandler.class);

    @Override
    public void handle(Store store, Collection<Change<RevisionMessage>> changes, Optional<RevisionMessage> message) {
        Collection<VersionResource> createdResourceVersions;
        Collection<VersionResource> modifiedResourceVersions;
        List<Change<RevisionMessage>> changesCreated = ChangeHelper.changesCreated(changes);
        List<Change<RevisionMessage>> changesModified = ChangeHelper.changesModified(changes);

        createdResourceVersions = versionsForCreatedChanges(changesCreated);
        modifiedResourceVersions = versionsForModifiedChanges(changesModified);

        RevisionMessage revisionMessage = message
                .orElseThrow(() -> new NullArgumentException("TRS needs a Service Provider info from the message"));
        //String spGraphKey = getVersionResourceGraphName(revisionMessage.getServiceProviderId());
        String spGraphKeyStr = getVersionResourceGraphName(revisionMessage.getServiceProviderId());
        try {
            URI spGraphKeyURI = new URI(spGraphKeyStr);
            if (!store.containsKey(spGraphKeyURI)) {
                store.putResources(spGraphKeyURI, createdResourceVersions);
                store.appendResources(spGraphKeyURI, modifiedResourceVersions);
            } else {
                store.appendResources(spGraphKeyURI, createdResourceVersions);
                store.appendResources(spGraphKeyURI, modifiedResourceVersions);
            }
        } catch (StoreAccessException e) {
            LOGGER.warn(String.valueOf(e));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    public static String getVersionResourceGraphName(String serviceProviderId) {
        String graphName = GitLabConstants.VR_GRAPH_NAME + "/" + serviceProviderId;
        LOGGER.debug("Graph name for '{}': '{}'", serviceProviderId, graphName);
        return graphName;
    }

    private VersionResource buildVersionResource(Revision revision, String serviceProviderId, AbstractResource r) {
        LOGGER.debug("Building a VR for {}@{}:{}", r, serviceProviderId, revision);
        try {
            String urlHash = DigestUtils.md5Hex(r.getAbout().toString());
            String revisionId = revision.getId();
            //VersionResource versionResource = new VersionResource(serviceProviderId, urlHash, revisionId);
            VersionResource versionResource = new VersionResource(serviceProviderId, revisionId);
            versionResource.setIdentifier(urlHash);
            versionResource.setVersionId(revisionId);
            // Why this method has not been generated?
            //versionResource.setWasRevisionOf(Sets.newHashSet(new Link(r.getAbout(), r.toString())));
            versionResource.setCreated(Date.from(revision.getTimestamp().toInstant()));
            return versionResource;
        } catch (URISyntaxException e) {
            LOGGER.warn(String.valueOf(e));
        }
        throw new IllegalArgumentException("Cannot construct a valid VersionResource");
    }

    private Collection<VersionResource> versionsForCreatedChanges(Collection<Change<RevisionMessage>> createdChanges) {
        return createdChanges.stream()
                .map(r -> buildVersionResource(r.getMessage().getRevision(), r.getMessage().getServiceProviderId(),
                        r.getResource())).collect(Collectors.toList());
    }

    private Collection<VersionResource> versionsForModifiedChanges(Collection<Change<RevisionMessage>> modifiedChanges) {
        // TODO 2016-09-19: create links between VersionResources
        return versionsForCreatedChanges(modifiedChanges);
    }
}
