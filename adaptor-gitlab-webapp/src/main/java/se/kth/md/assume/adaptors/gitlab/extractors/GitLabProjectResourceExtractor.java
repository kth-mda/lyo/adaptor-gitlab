package se.kth.md.assume.adaptors.gitlab.extractors;

import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.gitlab.api.models.GitlabIssue;
import org.gitlab.api.models.GitlabProject;
import org.gitlab.api.models.GitlabUser;
import se.kth.md.assume.adaptors.gitlab.GitLabConstants;
import se.kth.md.assume.adaptors.gitlab.GitLabManagerData;
import se.kth.md.assume.adaptors.gitlab.resources.ChangeRequest;
import se.kth.md.assume.adaptors.gitlab.resources.GitLabProject;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class GitLabProjectResourceExtractor {

    private GitLabManagerData gitLabManagerData;

    public GitLabProjectResourceExtractor(GitLabManagerData gitLabManagerData) {
        this.gitLabManagerData = gitLabManagerData;
    }

    public List<AbstractResource> extractGitLabProjects_init () {
        List<AbstractResource> resources = new ArrayList<>();

        // We retrieve the projects
        try {
            List<GitlabProject> projects = gitLabManagerData.getGitlabAPI().getProjects();

            for (GitLabManagerData.GitLabProjectInfos projectInfo : gitLabManagerData.getGitLabProjects().values()) {
                GitlabProject project = projectInfo.gitlabProject;

                System.out.println(project.getName() + " " + project.getId());

                try {
                    //GitLabProject p = new GitLabProject(GitLabConstants.PROJECT_MANAGEMENT_SERVICE_PROVIDER_ID,
                    //        project.getId().toString());
                    GitLabProject p = new GitLabProject(GitLabConstants.PROJECT_MANAGEMENT_SERVICE_PROVIDER_ID,
                            project.getName());

                    p.setTitle(project.getName());
                    String gitURL = project.getHttpUrl();
                    p.setGitURL(URI.create("http" + gitURL.substring(5)));
                    String webURL = project.getWebUrl();
                    p.setWebURL(URI.create("http" + webURL.substring(5)));

                    p.setCreated(project.getCreatedAt());

                    GitlabUser creator = gitLabManagerData.getOrRetrieveGitlabUser(project.getCreatorId().toString());
                    // TODO: link towards a Person URI instead
                    URI creatorURI = new URI(GitLabConstants.GITLAB_PLATFORM_PREFIX_URL + creator.getUsername());
                    p.setCreator(new Link(creatorURI));

                    resources.add(p);

                    HashSet<Link> issueLinks = new HashSet<>();
                    for (GitlabIssue issue : gitLabManagerData.getGitlabAPI().getIssues(project)) {
                        Integer issueID = new Integer(issue.getId());
                        URI issueURI = ChangeRequest.constructURI(GitLabConstants.CHANGE_MANAGEMENT_SERVICE_PROVIDER_ID,
                                issueID.toString());
                        issueLinks.add(new Link(issueURI));
                    }
                    p.setContainsChangeRequests(issueLinks);

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return resources;
    }

    public List<AbstractResource> extractGitLabProjects_update () {
        throw new UnsupportedOperationException ("Update of Gitlab projects not supported.");
    }

}
