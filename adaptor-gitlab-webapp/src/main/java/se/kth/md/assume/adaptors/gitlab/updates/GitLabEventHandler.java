package se.kth.md.assume.adaptors.gitlab.updates;

import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.tools.store.Store;
import org.eclipse.lyo.tools.store.StoreAccessException;
import org.eclipse.lyo.tools.store.update.Handler;
import org.eclipse.lyo.tools.store.update.change.Change;
import org.gitlab.api.GitlabAPI;
import se.kth.md.assume.adaptors.gitlab.GitLabConstants;
import se.kth.md.assume.adaptors.gitlab.GitLabManagerData;
import se.kth.md.assume.adaptors.gitlab.extractors.ResourceExtractorHelper;
import se.kth.md.assume.adaptors.gitlab.resources.ChangeRequest;
import se.kth.md.assume.adaptors.gitlab.resources.GitLabProject;
import se.kth.md.assume.adaptors.gitlab.servlet.ServiceProviderCatalogSingleton;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Created by Frederic Loiret on 2017-02-22.
 */
public class GitLabEventHandler implements Handler<GitlabEventMessage> {

    @Override
    public void handle(Store store, Collection<Change<GitlabEventMessage>> changes, Optional<GitlabEventMessage> message) {

        GitlabEventMessage m = message.get();

        GitLabManagerData gitLabManagerData = m.getGitLabManagerData();
        GitlabAPI gitlabAPI = gitLabManagerData.getGitlabAPI();

        for (Change<GitlabEventMessage> change : changes) {

            ChangeRequest cR = (ChangeRequest) change.getResource();

            GitLabProject project = null;
            if (m.getIssue_status().equals(GitlabEventMessage.ISSUE_STATUS.ISSUE_CREATED)) {
                ResourceExtractorHelper.createAndSetCreatorURI(cR, m.getIssue().getAuthor().getUsername());

                // TODO: same
                project = ResourceExtractorHelper.getGitLabProjectFromStore(gitLabManagerData.getStore(),
                        GitLabConstants.PROJECT_MANAGEMENT_SERVICE_PROVIDER_ID,
                        //m.getIssue().getProjectId());
                        m.getProjectName());

                Integer iID = new Integer(m.getIssueID());
                ResourceExtractorHelper.addIssueToProject(project, iID.intValue());
            }

            List<AbstractResource> cRList = new ArrayList<>();
            cRList.add(cR);
            List<AbstractResource> cPList = new ArrayList<>();
            cPList.add(project);

            URI cRSPURI = ServiceProviderCatalogSingleton.constructCmServicesServiceProviderURI(GitLabConstants.CHANGE_MANAGEMENT_SERVICE_PROVIDER_ID);
            try {
                store.appendResources(cRSPURI, cRList);
                if (project != null) {
                    URI pSPURI = ServiceProviderCatalogSingleton.constructPmServicesServiceProviderURI(GitLabConstants.PROJECT_MANAGEMENT_SERVICE_PROVIDER_ID);
                    store.appendResources(pSPURI, cPList);
                }
            } catch (StoreAccessException e) {
                e.printStackTrace();
            }
        }
    }
}
