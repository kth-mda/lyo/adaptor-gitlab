package se.kth.md.assume.adaptors.gitlab.updates;

import com.google.common.collect.Lists;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.tools.store.update.change.Change;
import org.eclipse.lyo.tools.store.update.change.ChangeKind;
import org.eclipse.lyo.tools.store.update.change.ChangeProvider;
import org.eclipse.lyo.tools.store.update.change.HistoryResource;
import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.GitlabIssue;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.assume.adaptors.gitlab.GitLabConstants;
import se.kth.md.assume.adaptors.gitlab.GitLabManagerData;
import se.kth.md.assume.adaptors.gitlab.extractors.ResourceExtractorHelper;
import se.kth.md.assume.adaptors.gitlab.repository.GitRepository;
import se.kth.md.assume.adaptors.gitlab.resources.ChangeRequest;
import se.kth.md.assume.repository.update.RevisionMessage;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Created by Frederic Loiret on 2017-02-21.
 */
public class GitlabChangeProviderImpl<T extends AbstractResource>
        implements ChangeProvider<GitlabEventMessage> {


    @Override
    public Collection<Change<GitlabEventMessage>> getChangesSince(Optional<ZonedDateTime> lastUpdate,
                                                                  Optional<GitlabEventMessage> message) {
        Collection<Change<GitlabEventMessage>> changes = new ArrayList<>();

        GitlabEventMessage m = message.get();

        GitLabManagerData gitLabManagerData = m.getGitLabManagerData();
        GitlabAPI gitlabAPI = gitLabManagerData.getGitlabAPI();

        if (m.getEvent().equals(GitlabEventMessage.GITLAB_EVENT.PUSH)) {
            GitLabManagerData.GitLabProjectInfos info = gitLabManagerData.getGitLabProjects().get(m.getProjectName());
            GitRepository gitRepo = info.gitRepo;

            gitLabManagerData.pullGitRepository(info.localRepo);

            List<Revision> revList = gitRepo.getRevisions();
            for (Revision rev : revList) {
                RevisionMessage msg = new RevisionMessage(info.serviceProviderId, rev);
                ZonedDateTime zd = rev.getTimestamp();
                Optional<RevisionMessage> rm = Optional.of(msg);
                info.updateManager.submit(zd, rm);
            }

        } else if (m.getEvent().equals(GitlabEventMessage.GITLAB_EVENT.ISSUE)) {
            Integer iID = new Integer(m.getIssueID());

            Change<GitlabEventMessage> change;

            GitlabIssue issue = null;
            try {
                issue = gitlabAPI.getIssue(m.getProjectID(), iID);
            } catch (IOException e) {
                e.printStackTrace();
            }

            ChangeRequest cR;
            ChangeKind cK;
            if (m.getIssue_status().equals(GitlabEventMessage.ISSUE_STATUS.ISSUE_CREATED)) {
                cR = ResourceExtractorHelper.createNewChangeRequest(GitLabConstants.CHANGE_MANAGEMENT_SERVICE_PROVIDER_ID,
                        issue.getId(), issue.getTitle(), issue.getDescription(), issue.getCreatedAt());
                cK = ChangeKind.CREATION;
            } else {
                cR = ResourceExtractorHelper.getChangeRequestFromStore(gitLabManagerData.getStore(),
                        GitLabConstants.CHANGE_MANAGEMENT_SERVICE_PROVIDER_ID,
                        issue.getId());
                cK = ChangeKind.MODIFICATION;
            }
            HistoryResource hR = new HistoryResource(cK, issue.getCreatedAt(), cR.getAbout());

            m.setIssue(issue);

            change = new Change<GitlabEventMessage>(cR, hR, m);

            return Lists.newArrayList(change);
        }

        // In the case of a PUSH event, we return null since the update will be handled by the update
        // manager associated to the updated git repository.
        return null;
    }
}

