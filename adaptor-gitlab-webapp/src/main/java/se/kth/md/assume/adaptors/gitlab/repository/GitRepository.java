package se.kth.md.assume.adaptors.gitlab.repository;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import org.eclipse.jgit.treewalk.filter.PathSuffixFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.aide.repository.api.Repository;
import se.kth.md.aide.repository.api.RepositoryAccessException;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.aide.repository.api.RevisionedObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * GitRepository is .
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 0.1.0
 */
public class GitRepository implements Repository {

    private final Logger logger = LoggerFactory.getLogger(se.kth.md.aide.repository.jgit.GitRepository.class);
    private final org.eclipse.jgit.lib.Repository repo;
    private final Git git;

    public GitRepository(String directory) throws IOException {
        File repoDirectory = new File(directory);
        git = Git.open(repoDirectory);
        repo = git.getRepository();
    }

    @Override
    public List<Revision> getRevisions() {
        try {
            Iterable<RevCommit> logs = git.log().call();
            Iterator<RevCommit> revCommitIterator = logs.iterator();
            Iterable<RevCommit> iterable = () -> revCommitIterator;
            Stream<RevCommit> commitStream = StreamSupport.stream(iterable.spliterator(), false);

            return commitStream.map(this::buildRevision).collect(Collectors.toList());
        } catch (GitAPIException e) {
            return new ArrayList<>();
        }
    }

    public List<RevisionedObject> getRevisionFiles(Revision rev, String fileExtension)
        throws RepositoryAccessException {
        try {
            ObjectId id = repo.resolve(rev.getId());
            ObjectReader reader = this.repo.newObjectReader();
            try {
                List<RevisionedObject> revisionedObjects = new ArrayList<>();
                RevWalk revWalk = new RevWalk(reader);
                RevCommit revCommit = revWalk.parseCommit(id);
                RevTree revTree = revCommit.getTree();

                TreeWalk treeWalk = new TreeWalk(repo);
                treeWalk.addTree(revTree);
                treeWalk.setRecursive(true);
                treeWalk.setFilter(PathSuffixFilter.create(fileExtension));

                while (treeWalk.next()) {
                    if (treeWalk.isSubtree()) {
                        logger.trace("dir: {}", treeWalk.getPathString());
                        treeWalk.enterSubtree();

                        //FileMode fileMode = treeWalk.getFileMode(0);
                        //System.out.println("src: " + getFileMode(fileMode) + ", type: " + fileMode.getObjectType() + ", mode: " + fileMode);

                    } else {
                        logger.trace("Parsing file '{}' that matches suffix '{}'", treeWalk.getPathString(),
                            fileExtension);

                        // see https://github.com/centic9/jgit-cookbook/blob/master/src/main/java/org/dstadler/jgit/api/GetFileAttributes.java
                        //FileMode fileMode = treeWalk.getFileMode(0);
                        //ObjectLoader loader = repo.open(treeWalk.getObjectId(0));
                        //System.out.println("README.md: " + getFileMode(fileMode) + ", type: " + fileMode.getObjectType() + ", mode: " + fileMode +
                        //        " size: " + loader.getSize());

                        RevisionedObject revisionedObject = new RevisionedObject(rev, treeWalk.getPathString());
                        revisionedObjects.add(revisionedObject);
                    }
                }
                return revisionedObjects;
            } finally {
                reader.close();
            }
        } catch (IOException e) {
            throw new RepositoryAccessException(e);
        }
    }

    private static String getFileMode(FileMode fileMode) {
        if (fileMode.equals(FileMode.EXECUTABLE_FILE)) {
            return "Executable File";
        } else if (fileMode.equals(FileMode.REGULAR_FILE)) {
            return "Normal File";
        } else if (fileMode.equals(FileMode.TREE)) {
            return "Directory";
        } else if (fileMode.equals(FileMode.SYMLINK)) {
            return "Symlink";
        } else {
            // there are a few others, see FileMode javadoc for details
            throw new IllegalArgumentException("Unknown type of file encountered: " + fileMode);
        }
    }

    @Override
    public InputStream getObjectStream(RevisionedObject revisionedObject) throws IOException {
        ObjectId id = repo.resolve(revisionedObject.getRevision().getId());
        ObjectReader reader = this.repo.newObjectReader();
        try {
            RevWalk revWalk = new RevWalk(reader);
            RevCommit revCommit = revWalk.parseCommit(id);
            RevTree revTree = revCommit.getTree();

            TreeWalk treeWalk = new TreeWalk(repo);
            treeWalk.addTree(revTree);
            treeWalk.setRecursive(true);
            treeWalk.setFilter(PathFilter.create(revisionedObject.getPath()));

            if (!treeWalk.next()) {
                throw new IllegalStateException("Did not find expected file 'README.md'");
            }

            logger.trace("Fetching object stream of '{}'", treeWalk.getPathString());
            ObjectId objectId = treeWalk.getObjectId(0);
            ObjectLoader loader = repo.open(objectId);
            return loader.openStream();
        } finally {
            reader.close();
        }
    }

    private Revision buildRevision(RevCommit commit) {
        return new Revision(getCommitId(commit), getTimestamp(commit));
    }

    private ZonedDateTime getTimestamp(RevCommit commit) {
        PersonIdent authorIdent = commit.getAuthorIdent();
        return authorIdent.getWhen().toInstant().atZone(authorIdent.getTimeZone().toZoneId());
    }

    private String getCommitId(RevCommit commit) {
        return commit.getId().getName();
    }
}
