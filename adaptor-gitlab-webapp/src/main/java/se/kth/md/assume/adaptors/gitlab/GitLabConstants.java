/*******************************************************************************
 * Copyright (c) 2012 IBM Corporation and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v. 1.0 which accompanies this distribution.
 *  
 * The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *
 *     Russell Boykin       - initial API and implementation
 *     Alberto Giammaria    - initial API and implementation
 *     Chris Peters         - initial API and implementation
 *     Gianluca Bernardini  - initial API and implementation
 *     Michael Fiedler      - Bugzilla adpater implementations
 *     Jad El-khoury        - initial implementation of code generator (https://bugs.eclipse.org/bugs/show_bug.cgi?id=422448)
 * 
 * This file is generated by org.eclipse.lyo.oslc4j.codegenerator
 *******************************************************************************/

package se.kth.md.assume.adaptors.gitlab;

import java.net.URI;

// Start of user code imports
// End of user code

public interface GitLabConstants
{
    // Start of user code user constants
    // see https://jena.apache.org/documentation/tdb/datasets.html for default graph URI
    URI TRS_GRAPH_NAME = URI.create("adaptor-trs-cache"); // from Jena docs
    // TODO: 2016-12-26 switch to a VR graph per SP
    String VR_GRAPH_NAME = "urn:x-arq:DefaultGraph"; // from Jena docs
    String PROJECT_MANAGEMENT_SERVICE_PROVIDER_ID = "PmSP";
    String CHANGE_MANAGEMENT_SERVICE_PROVIDER_ID = "CmSP";
    String PUSH_NOTIFICATIONS_SERVICE_PROVIDER_ID = "PnSP";
    String GITLAB_PLATFORM_PREFIX_URL = "https://gitlab.com/";
    // End of user code

    public static String CFGM_DOMAIN = "http://your.organisation.domain/nsp4#";
    public static String CFGM_NAMSPACE = "http://your.organisation.domain/nsp4#";
    public static String CFGM_NAMSPACE_PREFIX = "oslc_config";
    public static String CHANGE_MANAGEMENT_DOMAIN = "http://open-services.net/ns/cm#";
    public static String CHANGE_MANAGEMENT_NAMSPACE = "http://open-services.net/ns/cm#";
    public static String CHANGE_MANAGEMENT_NAMSPACE_PREFIX = "oslc_cm";
    public static String DUBLIN_CORE_DOMAIN = "http://purl.org/dc/terms/";
    public static String DUBLIN_CORE_NAMSPACE = "http://purl.org/dc/terms/";
    public static String DUBLIN_CORE_NAMSPACE_PREFIX = "dcterms";
    public static String FOAF_DOMAIN = "http://xmlns.com/foaf/0.1/";
    public static String FOAF_NAMSPACE = "http://xmlns.com/foaf/0.1/";
    public static String FOAF_NAMSPACE_PREFIX = "foaf";
    public static String FILE_MANAGEMENT_DOMAIN = "http://open-vocabulary.net/fm#";
    public static String FILE_MANAGEMENT_NAMSPACE = "http://open-vocabulary.net/fm#";
    public static String FILE_MANAGEMENT_NAMSPACE_PREFIX = "ov_fm";
    public static String PROJECT_MANAGEMENT_DOMAIN = "http://open-vocabulary.net/pm#";
    public static String PROJECT_MANAGEMENT_NAMSPACE = "http://open-vocabulary.net/pm#";
    public static String PROJECT_MANAGEMENT_NAMSPACE_PREFIX = "ov_pm";
    public static String PUSH_NOTIFICATIONS_DOMAIN = "http://your.organisation.domain/nsp12#";
    public static String PUSH_NOTIFICATIONS_NAMSPACE = "http://your.organisation.domain/nsp12#";
    public static String PUSH_NOTIFICATIONS_NAMSPACE_PREFIX = "notification";

    public static String CHANGEREQUEST = "ChangeRequest";
    public static String PATH_CHANGEREQUEST = "changeRequest";
    public static String TYPE_CHANGEREQUEST = CHANGE_MANAGEMENT_NAMSPACE + "ChangeRequest";
    public static String FILERESOURCE = "FileResource";
    public static String PATH_FILERESOURCE = "fileResource";
    public static String TYPE_FILERESOURCE = FILE_MANAGEMENT_NAMSPACE + "FileResource";
    public static String FOLDERRESOURCE = "FolderResource";
    public static String PATH_FOLDERRESOURCE = "folderResource";
    public static String TYPE_FOLDERRESOURCE = FILE_MANAGEMENT_NAMSPACE + "FolderResource";
    public static String GITLABPROJECT = "GitLabProject";
    public static String PATH_GITLABPROJECT = "gitLabProject";
    public static String TYPE_GITLABPROJECT = PROJECT_MANAGEMENT_NAMSPACE + "GitLabProject";
    public static String GITLABREPOSITORY = "GitLabRepository";
    public static String PATH_GITLABREPOSITORY = "gitLabRepository";
    public static String TYPE_GITLABREPOSITORY = PROJECT_MANAGEMENT_NAMSPACE + "GitLabRepository";
    public static String HOOKURL = "HookURL";
    public static String PATH_HOOKURL = "hookURL";
    public static String TYPE_HOOKURL = PUSH_NOTIFICATIONS_NAMSPACE + "HookURL";
    public static String PERSON = "Person";
    public static String PATH_PERSON = "person";
    public static String TYPE_PERSON = FOAF_NAMSPACE + "Person";
    public static String PROJECT = "Project";
    public static String PATH_PROJECT = "project";
    public static String TYPE_PROJECT = PROJECT_MANAGEMENT_NAMSPACE + "Project";
    public static String VERSIONRESOURCE = "VersionResource";
    public static String PATH_VERSIONRESOURCE = "versionResource";
    public static String TYPE_VERSIONRESOURCE = CFGM_NAMSPACE + "VersionResource";

    public static final String HDR_OSLC_VERSION = "OSLC-Core-Version";

}

