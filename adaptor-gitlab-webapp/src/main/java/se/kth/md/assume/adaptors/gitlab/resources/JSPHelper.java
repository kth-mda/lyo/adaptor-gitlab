package se.kth.md.assume.adaptors.gitlab.resources;

import org.eclipse.lyo.oslc4j.core.annotation.OslcName;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Frederic Loiret on 2017-02-19.
 */
public class JSPHelper {

    public static String getFooter (){
        return "GitLab Adaptor Demo";
    }


    public static String getResourceTypeName (AbstractResource resource) {
        for (Annotation a : resource.getClass().getAnnotations()) {
            if (a instanceof OslcName) {
                return ((OslcName)a).value();
            }
        }
        return null;
    }

    public static String getAbout (AbstractResource resource) {
        return createHTTPLinks(resource.getAbout().toString());
    }

    public static String toHtml (AbstractResource resource) {
        Object result = null;
        Class c = resource.getClass();
        try {
            Method m = c.getMethod("toHtml");
            try {
                result = m.invoke(resource);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return (String)result;
    }

    public static List<String> printHtmlResourceProperties (AbstractResource resource) {
        List<String> list = new ArrayList<>();

        for (OSLCAttInfo info : getOSLCAttributes(resource)) {
            Method toHtmlMethod = getToHtmlMethod (resource.getClass(), info.method);
            try {
                String html = (String)toHtmlMethod.invoke(resource);
                list.add(polishPropertyString(html));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        return orderPropertyList(list);
    }

    private static List<String> orderPropertyList (List<String> list ) {
        List<String> listWithURLs = new ArrayList<>();
        List<String> listWithoutURLs = new ArrayList<>();
        for (String s : list) {
            if (s.contains("http")) {
                listWithURLs.add(s);
            } else {
                listWithoutURLs.add(s);
            }
        }
        List<String> result = new ArrayList<>();
        result.addAll(listWithoutURLs);
        result.addAll(listWithURLs);
        return result;
    }

    private static class OSLCAttInfo {
        Method method;
        String propertyName;

        public OSLCAttInfo(Method method, String propertyName) {
            this.method = method;
            this.propertyName = propertyName;
        }
    }

    private static List<OSLCAttInfo> getOSLCAttributes (AbstractResource resource) {
        List<OSLCAttInfo> list = new ArrayList<>();
        Class c = resource.getClass();
        for (Method m : c.getMethods()) {
            for (Annotation a : m.getAnnotations()) {
                if (a instanceof OslcName) {
                    list.add(new OSLCAttInfo(m, ((OslcName) a).value()));
                }
            }
        }
        return list;
    }

    public static String polishPropertyString (String s) {
        String result = new String(s);
        result = result.replaceAll(":\\s", ":&nbsp;");

        return result;
    }


    public static String createHTTPLinks (String s) {
        ArrayList<String> ar = new ArrayList<>();
        String result = new String(s);

        String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(result);

        // The method matcher.matches() seems not to return true even when the pattern matches?
        boolean matched = false;

        int index = 0;
        while (matcher.find()) {
            int matchStart = matcher.start(1);
            int matchEnd = matcher.end();
            ar.add(result.substring(index, matchStart));
            String newUrl = "<a href=\"";
            String url = result.substring(matchStart, matchEnd);
            newUrl += url + "\">" + url + "</a>";
            ar.add(newUrl);
            matched = true;
            index = matchEnd;
        }
        if (matched) {
            ar.add(result.substring(index));
        } else {
            ar.add(result);
        }

        String returnResult = "";
        for (String str : ar) {
            returnResult += str;
        }

        return returnResult;
    }

    private static Method getToHtmlMethod (Class c, Method m) {
        String methodToFind = m.getName().substring(3) + "ToHtml";
        char ch[] = methodToFind.toCharArray();
        ch[0] = Character.toLowerCase(ch[0]);
        methodToFind = new String(ch);
        for (Method mm : c.getMethods()) {
            if (mm.getName().equals(methodToFind)) {
                return mm;
            }
        }
        return null;
    }

}
