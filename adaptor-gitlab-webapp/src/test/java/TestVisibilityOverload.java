import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

/**
 * TestVisibilityOverload is .
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-07-03
 */
public class TestVisibilityOverload {

    @Test
    public void testOverridenVisibility() {
        JavaVisibilityOverride sample = new JavaVisibilityOverride();
        String actual = getWork(sample);
        assertThat(actual, is(equalTo("private")));
        assertThat(sample.work(), is(equalTo("override")));
    }

    private String getWork(JavaPrivateSample sample) {
        return sample.getWork();
    }
}


class JavaPrivateSample {
    private String work() {
        return "private";
    }

    /**
     * This is an example of showing a method for internal use
     * that does not depend on object state
     * http://programmers.stackexchange.com/questions/229220/what-is-difference-between-protected-static-method-and-protected-method
     * @return
     */
    protected static int answer() {
        return 42;
    }

    public String getWork() {
        return work();
    }
}

class JavaVisibilityOverride extends JavaPrivateSample {

    public String work() {
        return "override";
    }

    public int getAnswer() {
        return JavaPrivateSample.answer(); // we are allowed to make this call
    }
}
